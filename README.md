# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

- There is 2 proposal for implement thread-safe container
  + using @ThreadSafe as original solution
  + using volatile key word for any data read by multiple thread-safe
  
- Provide some integration test support multithreading test process to ensure the correctness of start & stop functions

1 - Issue : 
	- If one thread is trying remove (in case want to update a member with same memberId but different age) Member while other writing to file.
	  java.util.ConcurrentModificationException error will be thrown
           --> Fix : provide setAge for Member class to update a member through addMember function if this memberId is existed
	(file : Member.java, line : 26
	file : PoorGroup.java, line : 42)
	- Member equals not has @Override annotation
	- Member hashcode not provided


2 - Enhancement : 

	- Make number of writing to be constant : 
	- Member class is stick with PoorGroup, hard for maintains and limit reusable
	--> Fix : Separate Member to another class for easy maintains and accessible
	(file : Member.java )
	- Every time starLog function called, it will clean up and start write again
	--> Fix : Open FileWriter with 2nd parameter = true to enable file append in order to write 10 times
	(file : PoorGroup.java , line : 106)

	- If call startLog multiple times (write to multiple file) and call stop will stop all file writing proccess
	--> Fix : Provide HashMap to store shouldStop flag for all writing process
				  Stop function will provide the filename need to stop writing
	(file : PoorGroup.java, line : 35)

	- Remove unnecessary convert from int to Integer 
	(file : PoorGroup.java, line : 69)
	- Using BufferedWriter to enhance performance
	(file : PoorGroup.java, line : 99)