package TreasureData;


import static org.junit.Assert.assertEquals;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.powermock.core.classloader.annotations.PrepareForTest;

import com.anarsoft.vmlens.concurrent.junit.ConcurrentTestRunner;
import com.anarsoft.vmlens.concurrent.junit.ThreadCount;

@RunWith(ConcurrentTestRunner.class)
@PrepareForTest(PoorGroupNonThreadSafeAnnotation.class)
public class PoorGroupNonThreadSafeAnnotationTesst {

	PoorGroupNonThreadSafeAnnotation pg;
	
	private final static int THREAD_COUNT = 1 ;
	private final static String PRIMARYFILE = "./primaryFileNonThreadSafe.txt";
	private final static String SECONDARYFILE = "./secondaryFileNonThreadSafe.txt";
	
	private final static String PRIMARYFILE1 = "./primaryFileNonThreadSafe1.txt";
	private final static String SECONDARYFILE1 = "./secondaryFileNonThreadSafe1.txt";
	

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		
		pg = new PoorGroupNonThreadSafeAnnotation("gr1");
		Member mb = new Member("mb1", 10);
		Member mb2 = new Member("mb2", 20);
		Member mb3 = new Member("mb3", 30);

		pg.addMember(mb);
		pg.addMember(mb2);
		pg.addMember(mb3);
		
	}

	@Test
	@ThreadCount(THREAD_COUNT)
	public void testStartLogging() {

		pg.startLoggingMemberList10Times(PRIMARYFILE, SECONDARYFILE);
		
		try {
			Thread.sleep(100);
			//Change age of the member with id = mb3 --> from the 2nd writing cycle, the member mb3 will be write with age is 99*10
			pg.addMember(new Member("mb3", 99));
			Thread.sleep(900);
			int lineCount = countLines(PRIMARYFILE);
			assertEquals(lineCount, 5*2*THREAD_COUNT);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test
	@ThreadCount(THREAD_COUNT)
	public void testStopLogging() {

		pg.startLoggingMemberList10Times(PRIMARYFILE, SECONDARYFILE);
		
		try {
			Thread.sleep(500);
			pg.stopPrintingMemberList(PRIMARYFILE);
			int lineCount = countLines(PRIMARYFILE);
			assertEquals(lineCount, 5*2*THREAD_COUNT);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	
	@After
	public void cleanUpFiles(){
		try {
			Thread.sleep(1100);
			File file1 = new File(PRIMARYFILE);
			File file2 = new File(SECONDARYFILE);
			File file3 = new File(PRIMARYFILE1);
			File file4 = new File(SECONDARYFILE1);
			if (file1 != null){
				file1.delete();
			}
			if (file2 != null) {
				file2.delete();
			}
			if (file3 != null) {
				file3.delete();
			}
			if (file4 != null) {
				file4.delete();
			}
			
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public int countLines(String filename) throws IOException {
	    InputStream is = new BufferedInputStream(new FileInputStream(filename));
	    try {
	        byte[] c = new byte[1024];
	        int count = 0;
	        int readChars = 0;
	        boolean empty = true;
	        while ((readChars = is.read(c)) != -1) {
	            empty = false;
	            for (int i = 0; i < readChars; ++i) {
	                if (c[i] == '\n') {
	                    ++count;
	                }
	            }
	        }
	        return (count == 0 && !empty) ? 1 : count;
	    } finally {
	        is.close();
	    }
	}

}
