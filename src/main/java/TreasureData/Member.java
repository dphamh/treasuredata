package TreasureData;

import java.util.Objects;

public class Member
{
    private String memberId;
    private int age;

    Member(String memberId, int age)
    {
        this.memberId = memberId;
        this.age = age;
    }

    public String getMemberId()
    {
        return memberId;
    }

    public int getAge()
    {
        return age;
    }
    
    public void setAge(int age)
    {
        this.age = age;
    }
    
    @Override
	public boolean equals(Object o) {
		// Should do some type-safe checks before compare the fields.
		if (o == this) return true;
		if (!(o instanceof Member)) return false;
		Member member = (Member) o;
		// If `memberId` matches the other's one, they should be treated as the same
		// `Member` objects.
		return Objects.equals(memberId, member.memberId);
	}
	/**
	 * Need to override the hashcode method when overriding equals method.
	 */
	@Override
	public int hashCode() {
		return Objects.hash(memberId);
	}
}