package TreasureData;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;

import javax.annotation.concurrent.ThreadSafe;

/**
 * A thread-safe container that stores a group ID and members.
 *
 * It can be added <tt>Member</tt> and return a member list as String. Also, it
 * can start and stop a background task that writes a member list to specified
 * files.
 *
 * This class is called a lot, so we need improve it.
 */
@ThreadSafe
public class PoorGroup {
	@SuppressWarnings("unused")
	private String groupId;
	private HashSet<Member> members;
	//Only stop writing for specific file instead of all file when call stop function
	private HashMap<String, Boolean> shouldStop;

	private static final int NUM_TIMES = 10;

	public PoorGroup(String groupId) {
		this.groupId = groupId;
		this.members = new HashSet<>();
		this.shouldStop = new HashMap<>();
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

	public void addMember(Member member) {
		if (members != null){
			if (members.contains(member)){
				Iterator<Member> iter = members.iterator();
	
				while (iter.hasNext()) {
				    Member runningMember = iter.next();
	
				    if (runningMember.getMemberId().equalsIgnoreCase(member.getMemberId())){
				    	runningMember.setAge(member.getAge());
				    }
				}
			} else {
				members.add(member);
			}
		}
	}
	

	public String getMembersAsStringWith10xAge() {
		StringBuilder buf = new StringBuilder();
		for (Member member : members) {
			// Shouldn't convert to Integer, it will take time to convert,
			// should use the primitive type
			// Integer age = member.getAge();
			int age = member.getAge();
			// Don't ask the reason why `age` should be multiplied ;)
			age *= 10;
			buf.append(String.format("memberId=%s, age=%d�n", member.getMemberId(), age));
		}
		buf.append("\n");
		return buf.toString();
	}

	/**
	 * Run a background task that writes a member list to specified files 10
	 * times in background thread so that it doesn't block the caller's thread.
	 */
	public void startLoggingMemberList10Times(final String outputFilePrimary, final String outputFileSecondary) {
		//Allow running for primary + secondary file
		if (shouldStop != null) {
			shouldStop.put(outputFilePrimary, Boolean.FALSE);
			shouldStop.put(outputFileSecondary, Boolean.FALSE);
		}
		new Thread(new Runnable() {
			@Override
			public void run() {
				//check if shouldStop value for files is false
				Boolean shouldStopPrimary = shouldStop.get(outputFilePrimary);
				Boolean shouldStopSecondary = shouldStop.get(outputFileSecondary);
				int i = 0;
				while (!shouldStopPrimary || !shouldStopSecondary) {
					if (i++ >= NUM_TIMES)
						break;
					FileWriter writer0 = null;
					FileWriter writer1 = null;
					// use buffer writer
					BufferedWriter bufWriter0 = null;
					BufferedWriter bufWriter1 = null;
					try {
						//only need for calling getMemberAsStringWith10xAge once
						String memberAsString = i + " : " + PoorGroup.this.getMembersAsStringWith10xAge();
						if (!shouldStopPrimary) {
							//open file with append indication option value is true for append to old file
							writer0 = new FileWriter(new File(outputFilePrimary), true);
							bufWriter0 = new BufferedWriter(writer0);
//							writer0.append(PoorGroup.this.getMembersAsStringWith10xAge());
							bufWriter0.append(memberAsString);
						}
						if (!shouldStopSecondary) {
							writer1 = new FileWriter(new File(outputFileSecondary), true);
							bufWriter1 = new BufferedWriter(writer1);
//							writer1.append(PoorGroup.this.getMembersAsStringWith10xAge());
							bufWriter1.append(memberAsString);
						}

					} catch (Exception e) {
						e.printStackTrace();
						throw new RuntimeException(
								"Unexpected error occurred. Please check these file names. outputFilePrimary="
										+ outputFilePrimary + ", outputFileSecondary=" + outputFileSecondary);
					} finally {
						try {
							if (bufWriter0 != null)
								bufWriter0.close();
							
							if (writer0 != null)
								writer0.close();

							if (bufWriter1 != null)
								bufWriter1.close();
							
							if (writer1 != null)
								writer1.close();
						//only need to catch specific IOException
//						} catch (Exception e) {
						} catch (IOException e) {
							e.printStackTrace();
							// Do nothing since there isn't anything we can do
							// here, right?
						}
					}

					try {
						Thread.sleep(100);
						//reload shouldStop values in case stop function is called from other thread
						shouldStopPrimary = shouldStop.get(outputFilePrimary);
						shouldStopSecondary = shouldStop.get(outputFileSecondary);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
		}).start();
	}

	/**
	 * Stop the background task started by <tt>startPrintingMemberList()</tt> Of
	 * course, <tt>startLoggingMemberList</tt> can be called again after calling
	 * this method.
	
	 */
	//Stop all writing task executed
	public void stopPrintingAll() {
		if (shouldStop != null) {
			for (String key : shouldStop.keySet()) {
				shouldStop.put(key, Boolean.TRUE);
			}
		}
	}

	//stop wiring task for specific file
	public void stopPrintingMemberList(final String outputFile) {
		if (shouldStop != null) {
			shouldStop.put(outputFile, Boolean.TRUE);
		}
	}
}